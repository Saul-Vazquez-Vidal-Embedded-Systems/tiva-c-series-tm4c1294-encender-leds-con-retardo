
#include <stdbool.h>            //BIBLIOTECA BASICA DE OPERACIONES BOOLEANAS
#include <stdint.h>             //BIBLIOTECA ESTANDAR DE TIPO DE DATOS
#include <inc/tm4c1294ncpdt.h>  //DEFINE EL NOMBRE DE LAS ETIQUETAS DE LOS PUERTOS, SUS DIRECCIONES Y NUMERO DE BITS

uint32_t i; //DECLARAMOS UNA VARIBLE QUE UTILIZAREMOS PARA HACER UN RETARDO DE 3 CICLOS DE RELOJ

int main(void)
{
    //---------------------------------- DEFINICION DE PUERTOS -----------------------------------
    SYSCTL_RCGCGPIO_R = 0x1100;     //HABILITAMOS LOS PUERTOS J Y N
    i = SYSCTL_RCGCGPIO_R;          //RETARDO DE 3 CICLOS DE RELOJ = 3*(1/16M) = 0.187us

    //----------------------------- CONFIGURACION DE LOS PUERTOS J Y N ---------------------------
    GPIO_PORTN_DIR_R = 0x03;        //DECLARAMOS LOS BITS 0 Y 1 DEL PUERTO N COMO SALIDAS
    GPIO_PORTN_DEN_R = 0x03;        //DEFINIMOS LOS BITS 0 Y 1 DEL PUERTO N COMO DIGITALES

    GPIO_PORTJ_AHB_DIR_R = 0x00;    //DECLARAMOS LOS BITS 0 Y 1 DEL PUERTO J COMO ENTRADAS
    GPIO_PORTJ_AHB_DEN_R = 0x03;    //DEFINIMOS LOS BITS 0 Y 1 DEL PUERTO J COMO DIGITALES
    GPIO_PORTJ_AHB_PUR_R = 0x03;    //DEFINIMOS RESISTENCIAS PULL UP PARA LOS ELEMENTOS DEL PUERTO J (LOS BOTONES)

    GPIO_PORTN_DATA_R = 0x00;        //APAGAMOS LOS LEDs CORRESPONDIENTES A PORTN0 Y PORTN1

    while(1){
        while((GPIO_PORTJ_AHB_DATA_R & 0x02) == 0x02){}; //ESPERAMOS A QUE SE PRESIONE EL BOTON DE PJ1 PARA INICIAR

        for(i = 0; i < 5; i++){
            GPIO_PORTN_DATA_R |= 0x02;  //ENCENDEMOS EL LED DE PN1
            SysCtlDelay(2666666);       //RETARDO DE 0.5 SEGUNDOS
            /*
             * SysCtlDelay() HACE RETARDOS DE CICLOS DE RELOJ.
             * Y COMO REQUIERE 3 INSTRUCCIONES PARA EJECUTARSE, DEBEMOS DIVIDIR LA FRECUENCIA ENTRE 3.
             * DE FORMA QUE OBTENEMOS UNA FORMULA ASI:
             *
             *          delay(nOfSec) = SysCtlDelay( (nOfSec) * (f_clock / 3) );
             *
             * DONDE
             *      nOfSec, ES LA CANTIDAD DE SEGUNDOS DE RETARDO,
             *      f_clock, ES LA FRECUENCIA DEL RELOJ, QUE EN ESTE CASO ES DE 16MHz
             *
             *DE FORMA QUE SI QUEREMOS UN DELAY DE 0.5 SEGUNDOS:
             *
             *      (nOfSec) * (f_clock / 3) = (0.5) * (16,000,000 / 3) = 2,666,666.6667
             */
            GPIO_PORTN_DATA_R &= ~0x02;     //APAGAMOS EL LED DE PN1
            SysCtlDelay(2666666);       //RETARDO DE 0.5 SEGUNDOS
        }
    }


}
